import {Header} from "./Header/Header.tsx";
import {Body} from "./Body/Body.tsx";
import {Footer} from "./Footer/Footer.tsx";

export const Skeleton = () => {
    return (
        <div
            style={{
                display: "flex",
                flexDirection: "column",
                gap: 30
            }}
        >
            <Header/>
            <Body/>
            <Footer/>
        </div>
    )
}