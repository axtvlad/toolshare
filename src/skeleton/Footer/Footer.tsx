export const Footer = () => {
    return (
        <footer
            style={{
                height: 60,
                width: '100%',
                backgroundColor: '#000',
                color: '#fff',
                alignItems: 'center',
                display: 'flex',
                borderRadius: 15
            }}
        >
            <div style={{padding: '0 20px'}}>
                В корзине {0} товаров
            </div>
        </footer>
    )
}