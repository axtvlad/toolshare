export const Header = () => {
    return (
        <header
            style={{
                height: 80,
                width: '100%',
                backgroundColor: "rgba(129,91,26,0.94)",
                borderRadius: 15,
            }}
        >
            <div
                style={{
                    display: 'flex',
                    justifyContent: "space-between",
                    alignItems: "center",
                    height: "inherit",
                    padding: '0 20px'
                }}
            >
                <span
                    style={{
                        fontSize: 30,
                        fontWeight: 'bold',
                        color: "white",
                    }}
                >
                    Tool Share
                </span>
                <div
                    style={{
                        display: 'flex',
                        justifyContent: "space-between",
                        gap: 60,
                    }}
                >
                    <span style={{cursor: 'pointer', color: 'white'}}>Главная</span>
                    <span style={{cursor: 'pointer', color: 'white'}}>Все товары</span>
                    <span style={{cursor: 'pointer', color: 'white'}}>Поиск</span>
                    <span style={{cursor: 'pointer', color: 'white'}}>Избранное</span>
                    <span style={{cursor: 'pointer', color: 'white'}}>Корзина</span>
                    <span style={{cursor: 'pointer', color: 'white'}}>Выход</span>
                </div>
            </div>
        </header>
    )
}