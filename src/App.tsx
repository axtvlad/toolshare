import './App.css'
import {Skeleton} from "./skeleton/Skeleton.tsx";

function App() {
    return (
        <>
            <Skeleton/>
        </>
    )
}

export default App
